class UsersController < ApplicationController
  def new
    @user = User.new
  end
  def index
    @users = User.all
  end
  
  def create
    @user = User.new(params[:user])
    if @user.save
      session[:user_id] = @user.id
      redirect_to  "/image/new", :notice => "Thanks for signing up!"
    else
      render "new"
    end
  end
end
