class Images < ActiveRecord::Base
  belongs_to :user
  has_many :votes, :foreign_key => :image_id
  has_many :comments, :foreign_key => :image_id

  validates_attachment_presence :image
  validates_attachment_size :image, :in => 1..3000000


  has_attached_file :image,
    :styles => { :thumb => "100x100>", :large => "800x800"},
    :storage => :s3,
      :s3_credentials => S3_CREDENTIALS,
      :path => ":attachment/:id/:style.:extension"

  def vote_exists(user)
    upvote_exists(user) || downvote_exists
  end
  def upvote_exists(user)
    return false if user.nil?
    self.votes.exists?(:user_id => user.id, :upvote => true)
  end

  def downvote_exists(user)
    return false if user.nil?
    self.votes.exists?(:user_id => user.id, :upvote => false)
  end

  def upvote
    self.upscore += 1
    self.save
  end
  def downvote
    self.downscore -= 1
    self.save
  end
  def score
    self.upscore
  end
  def self.ten_most_recent
    return order("created_at desc").limit(10)
  end
  def self.ten_most_votes
    return order("upscore desc").limit(10)
  end
end