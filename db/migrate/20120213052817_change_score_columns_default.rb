class ChangeScoreColumnsDefault < ActiveRecord::Migration
  def self.up
    change_column_default(:images, :upscore, 0)
    change_column_default(:images, :upscore, 0)
    Images.all.each do |i|
      i.upscore = 0 if i.upscore.nil?
      i.downscore = 0 if i.downscore.nil?
      i.save
    end
  end

  def self.down
      change_column_default(:images, :upscore, nil)
      change_column_default(:images, :upscore, nil)
  end
end