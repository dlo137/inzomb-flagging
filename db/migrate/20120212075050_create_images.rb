class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.integer :user_id
      t.string :caption
      t.integer :upscore
      t.integer :downscore

      t.timestamps
    end
  end
end
