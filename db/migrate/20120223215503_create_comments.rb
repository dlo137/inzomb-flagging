class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.text :comment_text
      t.integer :reply_id
      t.integer :image_id
      t.integer :user_id

      t.timestamps
    end
  end
end
